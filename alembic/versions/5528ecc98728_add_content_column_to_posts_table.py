"""add content column to posts table

Revision ID: 5528ecc98728
Revises: f71c816e12df
Create Date: 2022-10-14 16:11:42.099888

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5528ecc98728'
down_revision = 'f71c816e12df'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('posts', sa.Column('content', sa.String(), nullable=False))


def downgrade():
    op.drop_column('posts', 'content')
