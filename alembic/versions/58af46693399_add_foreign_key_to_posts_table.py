"""add foreign-key to posts table

Revision ID: 58af46693399
Revises: b03681cbf31e
Create Date: 2022-10-17 11:00:43.479281

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '58af46693399'
down_revision = 'b03681cbf31e'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('posts', sa.Column('owner_id', sa.Integer(), nullable=False))
    op.create_foreign_key('posts_users_fk', source_table="posts", referent_table="users",
                          local_cols=['owner_id'], remote_cols=['id'], ondelete="CASCADE")

def downgrade():
    op.drop_constraint('posts_users_fk', table_name="posts")
    op.drop_column("posts", 'owner_id')
